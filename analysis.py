import random,copy,time,os,csv,datetime,sys
import numpy as np


def lineage_fitness(df):

    # get the length of lineages
    T = df[df.lineage==0].birth_time.values[-1]
    lineages = df.lineage.unique() # get labels of lineages
    nc = np.mean([len(df[df.lineage==lin].birth_time.values) for lin in lineages])
    L0 = np.log(2)*nc/T
    L2 = np.log(np.mean([2.**(len(df[df.lineage==lin].birth_time.values)-nc) for lin in lineages]))/T
    return L0+L2

def gen_time_fitness(df):
    lineages = df.lineage.unique()
    return np.log(2.)/np.mean([np.mean(df[df.lineage==lin].birth_time.values) for lin in lineages])

def err(fitness_hat,fitness_true):
    return np.sqrt(np.mean([((f-fitness_true)/fitness_true)**2 for f in fitness_hat]))

def fitness_true_rgt(tau_avg,sigma_tau,cmd):
    return 2*np.log(2.)/tau_avg/(1.+np.sqrt(1.-2.*np.log(2.)*sigma_tau**2/tau_avg*(1+cmd)/(1-cmd)))
