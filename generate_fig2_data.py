import random,csv,os,copy,datetime
import numpy as np
import simulations as sims
import analysis as anl
from tqdm import tqdm

"""
This script tests the lineage algorithm for different paramater values
on the random generation time model
"""

# setup
# ----------------------------------------------------------------------

tmax=200
n_lineages = 1000
cmd_range = np.linspace(-0.5,0.6,5)
tau_avg = 1.




code_path = os.path.dirname(os.path.realpath(__file__))
out_path = code_path+"/figure_data/fig2"
os.makedirs(out_path, exist_ok=True)

# run for sigma = 0.01
# ----------------------------------------------------------------------


fitnesses_alg = []
fitnesses_true = []
sigma_tau = 0.1
for cmd in tqdm(cmd_range):
    sigma = sigma_tau*np.sqrt(1-cmd**2)
    df = sims.make_lineages_rgt(tmax,tau_avg,cmd,sigma,n_lineages)
    fitnesses_alg.append(anl.lineage_fitness(df))
    fitnesses_true.append(anl.fitness_true_rgt(tau_avg,sigma_tau,cmd))


np.savetxt(out_path+"/fitness_alg_sigma{}.txt".format(sigma_tau),fitnesses_alg)
np.savetxt(out_path+"/fitness_true_sigma{}.txt".format(sigma_tau),fitnesses_true)

# run for sigma = 0.015
# ----------------------------------------------------------------------

fitnesses_alg = []
fitnesses_true = []
sigma_tau = 0.15
for cmd in tqdm(cmd_range):
    sigma = sigma_tau*np.sqrt(1-cmd**2)
    df = sims.make_lineages_rgt(tmax,tau_avg,cmd,sigma,n_lineages)
    fitnesses_alg.append(anl.lineage_fitness(df))
    fitnesses_true.append(anl.fitness_true_rgt(tau_avg,sigma_tau,cmd))


np.savetxt(out_path+"/fitness_alg_sigma{}.txt".format(sigma_tau),fitnesses_alg)
np.savetxt(out_path+"/fitness_true_sigma{}.txt".format(sigma_tau),fitnesses_true)

# run for sigma = 0.02
# ----------------------------------------------------------------------

cmd_range = np.linspace(-0.5,0.6,5)
fitnesses_alg = []
fitnesses_true = []
sigma_tau = 0.2
for cmd in tqdm(cmd_range):
    sigma = sigma_tau*np.sqrt(1-cmd**2)
    df = sims.make_lineages_rgt(tmax,tau_avg,cmd,sigma,n_lineages)
    fitnesses_alg.append(anl.lineage_fitness(df))
    fitnesses_true.append(anl.fitness_true_rgt(tau_avg,sigma_tau,cmd))



np.savetxt(out_path+"/fitness_alg_sigma{}.txt".format(sigma_tau),fitnesses_alg)
np.savetxt(out_path+"/fitness_true_sigma{}.txt".format(sigma_tau),fitnesses_true)
