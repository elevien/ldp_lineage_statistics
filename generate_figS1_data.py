import random,csv,os,copy,datetime
import numpy as np
import simulations as sims
import analysis as anl
from tqdm import tqdm


"""
This script generates data for the distribution of the number of
cells in a growing population
"""

# set model paramaters
# ----------------------------------------------------------------------

tau_avg = 1. # average generation time
sigma_tau = 0.3 # variance in generation time
cmd = 0.2  # mother daughter correlations
sigma = sigma_tau*np.sqrt(1-cmd**2) # noise in langivan model


params = {
  "tau_avg": tau_avg,
  "sigma_tau":sigma_tau,
  "cmd":cmd
}

# simulate populations and save each in seperate file
# ----------------------------------------------------------------------
n_samples = 100
Nmax = 10**10
tmax = 14.

code_path = os.path.dirname(os.path.realpath(__file__))
out_path = code_path+"/figure_data/figS1"
os.makedirs(out_path, exist_ok=True)

for k in tqdm(range(n_samples)):
    seed  = 1.
    N,T,L = sims.make_population_rgt(Nmax,tau_avg,cmd,sigma,seed,dt_sample=0.01,tmax=tmax)
    np.savetxt(out_path+"/N%i.txt"%k,N)
    np.savetxt(out_path+"/T%i.txt"%k,T)
