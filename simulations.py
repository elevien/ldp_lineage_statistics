import random,copy,os
import numpy as np
import pandas as pd
from scipy import optimize

#------------------------------------------------------------------------------
# generate independent lineages
#------------------------------------------------------------------------------
def make_lineages_rgt(tmax,t_avg,cmd,sigma,n_lineages):
    '''
    Make lineages using random generation time model

    Input:
        tmax        - the maximum time to run n_lineages
        t_avg       - the average generation time
        cmd         - the correlation between mother and daughter generation times
        sigma       - the time additive noise
        n_lineages  - the number of lineages

    Output:
        df          - a DataFrame containing birth times, gen times and lineage names

    '''

    birth_time = []
    gen_time = []
    lineage = []
    for k in range(n_lineages):
        t_cur = 0.
        t_gen = t_avg
        while t_cur<tmax:
            birth_time.append(t_cur)
            t_gen = t_gen*cmd + t_avg*(1-cmd) + np.random.normal(0,sigma)
            t_cur += t_gen
            gen_time.append(t_gen)
            lineage.append(k)

    return pd.DataFrame({'birth_time':birth_time,'gen_time':gen_time,'lineage':lineage})


def make_lineages_csr(tmax,a,sigmav,sigmagr,n_lineages):
    '''
    Make lineages using cell-size regulation model

    Input:
        tmax        - the maximum time to run n_lineages
        a           - cell-size regulation paramater
        sigmav      - size addative noise paramater
        sigmagr     - growth rate noise paramater
        n_lineages  - the number of lineages

    Output:
        df          - a DataFrame containing birth times, gen times and lineage names

    '''

    def make_gen_time(vb,gr):
        xiv = np.random.normal(0.,sigmav)
        vf = 2*(1-a)*vb+2*a + xiv
        return np.log(vf/vb)/gr

    birth_time = []
    gen_time = []
    lineage = []
    vb = 1. # initial cell's birth volume
    for k in range(n_lineages):
        t_cur = 0.
        t_gen = np.log(2)
        while t_cur<tmax:
            gr = 1.0+np.random.normal(0.0,sigmagr)
            birth_time.append(t_cur)
            t_gen = make_gen_time(vb,gr)
            vb = vb*np.exp(gr*t_gen)/2. # next cell's birth volume
            t_cur += t_gen
            gen_time.append(t_gen)
            lineage.append(k)

    return pd.DataFrame({'birth_time':birth_time,'gen_time':gen_time,'lineage':lineage})

#------------------------------------------------------------------------------
# generate population dynamics
#------------------------------------------------------------------------------

def make_population_rgt(Nmax,tau_avg,cmd,sigma,seed,*,dt_sample=0.01,tmax=100):
    '''
    Simulate exponentially growing population using random generation times model
    Also computes the fitness

    Input:
        Nmax        - the maximum number of cells to generate
        t_avg       - the average generation time
        cmd         - the correlation between mother and daughter generation times
        sigma       - the time additive noise paramater
        seed        - generation time of ancestral cell
                        (relevant if there are correlations)

    Optional Input:
        tmax        - the maximum time to run
        dt_sample   - frequancy to save samples of the number of cells


    Output:
        N          - the number of cells at sample times
        T          - sample times
        L          - estimate of population growth rate from fitting
    '''
    gt0 = seed*cmd + tau_avg*(1-cmd)+np.random.normal(0,sigma)
    gt  = seed*cmd + tau_avg*(1-cmd)+np.random.normal(0,sigma)
    #cells =[Cell(gt,gt)]
    cells_gt = np.zeros(Nmax) # generation times
    cells_dt = np.zeros(Nmax) # division times
    cells_gt[0] = gt
    cells_dt[0] = gt
    N = [1]
    n=1
    T = [0.]
    t_sample = 0.
    n = 1
    t = 0.

    while n<Nmax and t<tmax:
        #cells.sort(key = lambda Cell : Cell.div_time) # can this be made more efficient?
        ind = np.argmin(cells_dt[0:n])
        mother_dt = cells_dt[ind]
        mother_gt = cells_gt[ind]
        t_next = mother_dt
        gt1 = mother_gt*cmd + tau_avg*(1-cmd)+np.random.normal(0,sigma)
        gt2 = mother_gt*cmd + tau_avg*(1-cmd)+np.random.normal(0,sigma)
        cells_gt[ind] = gt1
        cells_dt[ind] = gt1+t_next
        cells_gt[n] = gt2
        cells_dt[n] = gt2+t_next

        t_sample += t_next-t
        t = t_next
        n = n+1

        # save samples
        t_last = T[-1]
        while t-t_last>dt_sample:
            t_last += dt_sample
            T.append(t_last)
            N.append(n)
            t_sample = 0.

    # compute population growth rate
    def fit_func(x,a,b):
        return a*x+b
    fit_steps = len(T)//2 # fit to second half of time
    fits,errs = optimize.curve_fit(fit_func,T[fit_steps:-1],[np.log(n) for n in N[fit_steps:-1]])
    L = fits[0]
    if n>Nmax:
        print("Warning: n>Nmax")
    return N,T,L

def make_population_csr(Nmax,a,sigmav,sigmagr,seed,*,dt_sample=0.01,tmax=100):
    '''
    Simulate exponentially growing population using cell-size regulation model
    Also computes the fitness

    Input:
        Nmax        - the maximum number of cells to generate
        a           - cell-size regulation paramater
        sigmav      - size addative noise paramater
        sigmagr     - growth rate noise paramater
        seed        - generation time of ancestral cell
                        (relevant if there are correlations)

    Optional Input:
        tmax        - the maximum time to run
        dt_sample   - frequancy to save samples of the number of cells


    Output:
        N          - the number of cells at sample times
        T          - sample times
        L          - estimate of population growth rate from fitting
    '''
    def gen_time(vb,gr):
        xiv = np.random.normal(0.,sigmav)
        vf = 2*(1-a)*vb+2*a + xiv
        return np.log(vf/vb)/gr

    #cells =[Cell(gt,gt)]
    cells_gt = np.zeros(Nmax) # generation times
    cells_dt = np.zeros(Nmax) # division times
    cells_vb = np.zeros(Nmax)  # birth volumes
    cells_gr = np.zeros(Nmax)  # growth rates
    cells_vb[0] = 1.
    cells_gr[0] = 1.+np.random.normal(0.,sigmagr)
    cells_gt[0] = gen_time(cells_vb[0],cells_gr[0])
    cells_dt[0] = cells_gt[0]

    gen_times = np.zeros(2*Nmax)

    N = [1]
    n=1
    T = [0.]
    t_sample = 0.
    n = 1 # current number of cells
    n_tree = 1 # number of cells on entire tree
    t = 0.

    while n<Nmax and t<tmax:

        #cells.sort(key = lambda Cell : Cell.div_time) # can this be made more efficient?
        ind = np.argmin(cells_dt[0:n])
        mother_dt = cells_dt[ind]
        mother_gt = cells_gt[ind]
        mother_vb = cells_vb[ind]
        mother_gr = cells_gr[ind]
        mother_vf = mother_vb*np.exp(mother_gr*mother_gt)
        t_next = mother_dt
        gen_times[n_tree-1] = mother_gt

        cells_vb[ind] = mother_vf/2.
        cells_gr[ind] = 1.+np.random.normal(0.,sigmagr)
        cells_gt[ind] = gen_time(cells_vb[ind],cells_gr[ind])
        cells_dt[ind] = t_next + cells_gt[ind]


        cells_vb[n] = mother_vf/2.
        cells_gr[n] = 1.+np.random.normal(0.,sigmagr)
        cells_gt[n] = gen_time(cells_vb[ind],cells_gr[ind])
        cells_dt[n] = t_next + cells_gt[ind]

        t_sample += t_next-t
        t = t_next
        n = n+1
        n_tree = n_tree + 1

        # save samples
        t_last = T[-1]
        while t-t_last>dt_sample:
            t_last += dt_sample
            T.append(t_last)
            N.append(n)
            t_sample = 0.

    # compute population growth rate
    def fit_func(x,a,b):
        return a*x+b
    fit_steps = len(T)//2 # fit to second half of time
    fits,errs = optimize.curve_fit(fit_func,T[fit_steps:-1],[np.log(n) for n in N[fit_steps:-1]])
    L = fits[0]
    if n>Nmax:
        print("Warning: n>Nmax")
    return np.array(N),np.array(T),L
