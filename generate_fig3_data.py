import random,csv,os,copy,datetime
import numpy as np
import simulations as sims
import analysis as anl
from tqdm import tqdm


"""
This script generates covergence data for lineage representation using
random generation time model
"""

# set model paramaters
# ----------------------------------------------------------------------

tau_avg = 1. # average generation time
sigma_tau = 0.2 # variance in generation time
cmd = 0.1  # mother daughter correlations
sigma = sigma_tau*np.sqrt(1-cmd**2) # noise in langivan model

params = {
  "tau_avg": tau_avg,
  "sigma_tau":sigma_tau,
  "cmd":cmd
}


# simulate lineages
# ----------------------------------------------------------------------

n_lineages = 200
tmax = 1000
n_samples = 10 # number of experiments to run
samples = [] # this will be a list of data frames, each storing a sample
print('generating lineages ...')
for s in tqdm(range(n_samples)):
    samples.append(sims.make_lineages_rgt(tmax,tau_avg,cmd,sigma,n_lineages))

# compute the fitness using different numbers of lineages
# ----------------------------------------------------------------------

nT = 10 # number of different durations to compute
nM = 10 # number of different durations to compute
t_range = np.logspace(0.5,2,nT)               # max times
M_range = np.logspace(0.5,2,nM,dtype=int)     # number of lineages

fitness_true = anl.fitness_true_rgt(tau_avg,sigma_tau,cmd)
Err_lin = np.zeros((nT,nM))

print('computing fitness ...')
for k in tqdm(range(nT)):
    for j in range(nM):
        t,m = t_range[k],M_range[j]
        fitnesses = []
        for s in range(n_samples):
            df = samples[s]
            df_trunc = df[(df.lineage<m) & (df.birth_time<t)]
            fitnesses.append(anl.lineage_fitness(df_trunc))
        Err_lin[k,j] = anl.err(fitnesses,fitness_true)



# save data
# ----------------------------------------------------------------------

code_path = os.path.dirname(os.path.realpath(__file__))
out_path = code_path+"/figure_data/fig3"
os.makedirs(out_path, exist_ok=True)

w = csv.writer(open(out_path+"/params.csv", "w"))
w.writerow([datetime.datetime.now()])
w.writerow(["output",id])
for key, val in params.items():
   w.writerow([key, val])


np.savetxt(out_path+"/T_range_short.txt",t_range)
np.savetxt(out_path+"/M_range_short.txt",M_range)
np.savetxt(out_path+"/Err_lin_short.txt",Err_lin)
