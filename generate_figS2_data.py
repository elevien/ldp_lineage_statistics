import random,csv,os,copy,datetime
import numpy as np
import simulations as sims
import analysis as anl
from tqdm import tqdm


"""
This script generates covergence data for distributions (in particular, of
gamma) and other statistics computed from the lineage
"""

# set model paramaters
# ----------------------------------------------------------------------


tau_avg = 1. # average generation time
sigma_tau = 0.2 # variance in generation time
cmd = 0.2  # mother daughter correlations
sigma = sigma_tau*np.sqrt(1-cmd**2) # noise in langivan model

params = {
  "tau_avg": tau_avg,
  "sigma_tau":sigma_tau,
  "cmd":cmd
}


# simulate lineages
# ----------------------------------------------------------------------

tmax = 2*10**2
n_lineages = 1000
df = sims.make_lineages_rgt(tmax,tau_avg,cmd,sigma,n_lineages)
lineages = df.lineage.unique()

# compute statistics of gamma
# ----------------------------------------------------------------------
nT = 180
t_range = np.linspace(10,tmax,nT)

gam = np.zeros((nT,n_lineages))
var_gam = np.zeros(nT)
mean_2gam = np.zeros(nT)
var_2gam = np.zeros(nT)

for k in tqdm(range(nT)):
    t= t_range[k]


    gam[k,:] = [len(df[(df.lineage==lin) & (df.birth_time<t)].birth_time.values)/t for lin in lineages]

    var_gam[k] = np.var(gam[k,:])
    var_2gam[k] = np.var(2**(t*gam[k,:]))
    mean_2gam[k] = np.mean(2**(t*gam[k,:]))

# save data
# ----------------------------------------------------------------------

# setup directories
code_path = os.path.dirname(os.path.realpath(__file__))
out_path = code_path+"/figure_data/figS2"
os.makedirs(out_path, exist_ok=True)

w = csv.writer(open(out_path+"/params.csv", "w"))
w.writerow([datetime.datetime.now()])
w.writerow(["output",id])
for key, val in params.items():
   w.writerow([key, val])

np.savetxt(out_path+"/t_range.txt",t_range)
np.savetxt(out_path+"/gam.txt",gam)
np.savetxt(out_path+"/var_gam.txt",var_gam)
np.savetxt(out_path+"/var_2gam.txt",var_2gam)
np.savetxt(out_path+"/mean_2gam.txt",mean_2gam)
